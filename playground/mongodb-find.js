const {MongoClient, ObjectID} = require('mongodb');


MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
  if (err) {
    return console.log('Unable to connect to mongodb server');
  }
  const db = client.db('TodoApp');
  console.log('Connected to mongodb server');

  // db.collection('Todos').find({
  //   _id: new ObjectID('5a8561f09660db0ea3a08c9e')
  // }).toArray().then((docs) => {
  //   console.log('Todos');
  //   console.log(JSON.stringify(docs, undefined, 2));
  // }, (err) => {
  //   console.log('Unable to fetch todos', err);
  // });

  // db.collection('Todos').find().count().then((count) => {
  //   console.log('Todos count: ', count);
  // }, (err) => {
  //   console.log('Unable to fetch todos', err);
  // });

  db.collection('Users').find({
    name: 'Jinny'
  }).toArray().then((docs) => {
    console.log(JSON.stringify(docs, undefined, 2));
  });

  // client.close();
});
