const {MongoClient, ObjectID} = require('mongodb');


MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, client) => {
  if (err) {
    return console.log('Unable to connect to mongodb server');
  }
  const db = client.db('TodoApp');
  console.log('Connected to mongodb server');

  // db.collection('Todos').deleteMany({text: 'Eat lunch'}).then((result) => {
  //   console.log(result);
  // });

  // db.collection('Todos').deleteOne({text: 'Eat lunch'}).then((result) => {
  //   console.log(result);
  // });

  // db.collection('Todos').findOneAndDelete({completed: false}).then((result) => {
  //   console.log(result);
  // });

  // db.collection('Users').deleteMany({name: 'Panupong'});

  db.collection('Users').findOneAndDelete({_id: new ObjectID('5a8564b9ef8ed60ebbac735c')})
  .then((result) => {
    console.log(JSON.stringify(result, undefined, 2));
  });

  // client.close();
});
